<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ColorsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultColor = env(
            "DEFAULT_COLORS",
            [
                'primary',
                'secondary',
                'accent',
                'neutral',
                'base-100',
                'info',
                'success',
                'warning',
                'error',
            ]
        );

        foreach ($defaultColor as $color) {
            Color::create(
                [
                    'hex' => $color
                ]
            );
        }
    }
}

## Laravel 9 + VueJs 3 + Inertia + Sail

### Definições
- Projeto para criação e gerenciamento de eventos
- Interface com tema definido e aplicado em cada tela.
- Auth built in do Laravel com Sanctum
- Conceito a ser aplicado na estrutura é o DDD
- Testes de Features para cada funcionalidade
- TailwindCss e DaisyUi para o front integrado ao VueJs 3 e Inertia para controle de requisições e propriedades transportadas pelo backend embutido Laravel 9

### TODOS:
1. Adicionar calendário na tela de eventos.
2. Adicionar crud para tipos de eventos e eventos em sí.
3. Adicionar integração com calendly
4. Coverage 100% da aplicação
